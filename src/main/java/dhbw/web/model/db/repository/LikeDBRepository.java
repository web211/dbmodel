package dhbw.web.model.db.repository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import dhbw.web.model.db.LikeDB;

public class LikeDBRepository extends StaticDB<LikeDB> {

	public LikeDB findByUserAndElement(int userId, int element, String type) {
		try {

			return getDB().stream()
					.filter(x -> x.getUser() == userId && x.getParentId() == element && x.getParentType().equals(type))
					.findFirst().get();

		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public List<LikeDB> findByElement(int element, String type) {
		return getDB().stream().filter(x -> x.getParentId() == element && x.getParentType().equals(type))
				.collect(Collectors.toList());
	}

}
