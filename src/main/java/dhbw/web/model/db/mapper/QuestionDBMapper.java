package dhbw.web.model.db.mapper;

import dhbw.web.model.Question;
import dhbw.web.model.User;
import dhbw.web.model.db.QuestionDB;

public class QuestionDBMapper extends ModelDBSelfreference<Question, QuestionDB> {

	@Override
	public QuestionDB map(Question o) {
		QuestionDB t = new QuestionDB();
		t.setTitle(o.getTitle());
		t.setText(o.getText());
		t.setUrl(o.getUrl());
		t.setOwner(o.getOwner().getId());
		t.setCreated(o.getCreated());
		t.setId(o.getId());
		return t;
	}

	@Override
	public Question revert(QuestionDB t) {
		Question o = new Question();
		o.setTitle(t.getTitle());
		o.setText(t.getText());
		o.setUrl(t.getUrl());
		User u = new User();
		u.setId(t.getOwner());
		o.setOwner(u);
		o.setCreated(t.getCreated());
		o.setId(t.getId());
		return o;
	}
}
