package dhbw.web.model.db.repository;

import java.util.List;
import java.util.stream.Collectors;

import dhbw.web.model.db.NotificationDB;

public class NotificationDBRepository extends StaticDB<NotificationDB> {

	public List<NotificationDB> readLatest(int amount, List<Integer> follows) {
		return readLatestAll(follows).stream().sorted((x, y) -> x.getCreated().compareTo(y.getCreated())).limit(amount)
				.collect(Collectors.toList());
	}

	public List<NotificationDB> readLatestAll(List<Integer> follows) {
		return getDB().stream().filter(x -> follows.stream().anyMatch(y -> y == x.getFollow()))
				.collect(Collectors.toList());
	}

}
