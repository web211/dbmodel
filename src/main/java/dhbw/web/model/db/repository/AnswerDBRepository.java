package dhbw.web.model.db.repository;

import java.util.List;
import java.util.stream.Collectors;

import dhbw.web.model.db.AnswerDB;

public class AnswerDBRepository extends StaticDB<AnswerDB> {

	public List<AnswerDB> findQuestionAnswers(int question) {
		return getDB().stream().filter(x -> x.getParentQuestion() == question).collect(Collectors.toList());
	}
}
