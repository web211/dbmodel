package dhbw.web.model.db.mapper;

import dhbw.web.model.ElementParentImpl;
import dhbw.web.model.Like;
import dhbw.web.model.User;
import dhbw.web.model.db.LikeDB;

public class LikeDBMapper extends ModelDBSelfreference<Like, LikeDB> {
	@Override
	public LikeDB map(Like o) {
		LikeDB t = new LikeDB();
		t.setUser(o.getLiked().getId());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		t.setCreated(o.getCreated());
		t.setId(o.getId());
		return t;
	}

	@Override
	public Like revert(LikeDB t) {
		User u = new User();
		u.setId(t.getUser());
		Like o = new Like(u, new ElementParentImpl(t.getParentId(), t.getParentType()));
		o.setCreated(t.getCreated());
		o.setId(t.getId());
		return o;
	}
}
