package dhbw.web.model.db.repository;

import java.util.NoSuchElementException;

import dhbw.web.model.db.UserDB;

public class UserDBRepository extends StaticDB<UserDB> {

	public UserDB findByUsername(String username) {
		try {
			return getDB().stream().filter(x -> x.getUsername().equals(username)).findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

}
