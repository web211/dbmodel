package dhbw.web.model.db.mapper;

import dhbw.web.model.Answer;
import dhbw.web.model.Question;
import dhbw.web.model.User;
import dhbw.web.model.db.AnswerDB;

public class AnswerDBMapper extends ModelDBSelfreference<Answer, AnswerDB> {

	@Override
	public AnswerDB map(Answer o) {
		AnswerDB t = new AnswerDB();
		t.setId(o.getId());
		t.setText(o.getText());
		t.setCreated(o.getCreated());
		t.setOwner(o.getOwner().getId());
		t.setParentQuestion(o.getParentQuestion().getId());

		t.setId(o.getId());
		return t;
	}

	@Override
	public Answer revert(AnswerDB t) {
		Answer o = new Answer();
		o.setId(t.getId());
		o.setText(t.getText());
		o.setCreated(t.getCreated());

		User u = new User();
		u.setId(t.getOwner());
		o.setOwner(u);
		Question q = new Question();
		q.setId(t.getParentQuestion());
		o.setParentQuestion(q);
		o.setId(t.getId());
		return o;
	}

}
