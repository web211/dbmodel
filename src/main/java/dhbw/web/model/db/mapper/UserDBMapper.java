package dhbw.web.model.db.mapper;

import dhbw.web.model.User;
import dhbw.web.model.db.UserDB;

public class UserDBMapper extends ModelDBSelfreference<User, UserDB> {

	@Override
	public UserDB map(User o) {
		UserDB t = new UserDB();
		t.setUsername(o.getUsername());
		t.setEmail(o.getEmail());
		t.setFirstname(o.getFirstname());
		t.setLastname(o.getLastname());
		t.setPassword(o.getPassword());
		t.setLastLogon(o.getLastLogon());
		t.setId(o.getId());
		return t;
	}

	@Override
	public User revert(UserDB t) {
		User o = new User();
		o.setUsername(t.getUsername());
		o.setEmail(t.getEmail());
		o.setFirstname(t.getFirstname());
		o.setLastLogon(t.getLastLogon());
		o.setLastname(t.getLastname());
		o.setPassword(t.getPassword());
		o.setId(t.getId());
		return o;
	}
}
