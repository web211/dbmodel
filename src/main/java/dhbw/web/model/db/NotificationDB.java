package dhbw.web.model.db;

import java.util.Date;

import dhbw.web.model.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NotificationDB extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date created;
	private String text;
	private int follow;
	private boolean seen;
}
