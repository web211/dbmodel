package dhbw.web.model.db;

import java.util.Date;

import dhbw.web.model.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class LikeDB extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int user;
	private int parentId;
	private String parentType;
	private Date created;

}
