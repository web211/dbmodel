package dhbw.web.model.db.mapper;

import dhbw.web.utils.MapperWrapper;

public class ModelDBMapper {

	private static MapperWrapper mw;

	public static MapperWrapper getModelDBMapper() {
		if (mw == null) {
			mw = new MapperWrapper("dhbw.web.model.db.mapper");
		}
		return mw;
	}

	private ModelDBMapper() {

	}

}
