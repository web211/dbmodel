package dhbw.web.model.db.mapper;

import dhbw.web.model.ElementParentImpl;
import dhbw.web.model.Follow;
import dhbw.web.model.User;
import dhbw.web.model.db.FollowDB;

public class FollowDBMapper extends ModelDBSelfreference<Follow, FollowDB> {

	@Override
	public FollowDB map(Follow o) {
		FollowDB t = new FollowDB();
		t.setCreated(o.getCreated());
		t.setUser(o.getFollower().getId());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		t.setId(o.getId());
		return t;

	}

	@Override
	public Follow revert(FollowDB t) {
		Follow o = new Follow();
		o.setCreated(t.getCreated());
		User u = new User();
		u.setId(t.getUser());
		o.setFollower(u);
		o.setParent(new ElementParentImpl(t.getParentId(), t.getParentType()));
		o.setId(t.getId());
		return o;
	}
}
