package dhbw.web.model.db.mapper;

import dhbw.web.model.Comment;
import dhbw.web.model.ElementParentImpl;
import dhbw.web.model.User;
import dhbw.web.model.db.CommentDB;

public class CommentDBMapper extends ModelDBSelfreference<Comment, CommentDB> {

	@Override
	public CommentDB map(Comment o) {
		CommentDB t = new CommentDB();
		t.setText(o.getText());
		t.setCreated(o.getCreated());
		t.setParentId(o.getParent().getId());
		t.setParentType(o.getParent().getType());
		t.setOwner(o.getOwner().getId());
		t.setId(o.getId());
		return t;
	}

	@Override
	public Comment revert(CommentDB t) {
		Comment o = new Comment();
		o.setCreated(t.getCreated());
		o.setText(t.getText());
		User u = new User();
		u.setId(t.getOwner());
		o.setOwner(u);
		o.setParent(new ElementParentImpl(t.getParentId(), t.getParentType()));
		o.setId(t.getId());
		return o;
	}
}
