package dhbw.web.model.db.repository;

import java.util.List;
import java.util.NoSuchElementException;

import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Entity;
import lombok.Synchronized;

public class StaticDB<T extends Entity> {

	private List<T> _q;
	private int _seq = 1;

	public List<T> getDB() {
		if (_q == null) {
			_q = ModelFactory.getFactory().getList();
		}
		return _q;
	}

	@Synchronized
	public int save(T e) {
		T existing = findById(e.getId());
		if (existing == null) {
			e.setId(getNextId());
		} else {
			e.setId(existing.getId());
			getDB().remove(existing);
		}
		getDB().add(e);
		return e.getId();
	}

	@Synchronized
	public T findById(int id) {
		try {
			return getDB().stream().filter(x -> x.getId() == id).findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	@Synchronized
	public void deleteById(int id) {
		T existing = findById(id);
		if (existing != null) {
			getDB().remove(existing);
		}
	}

	@Synchronized
	public Iterable<T> findAll() {
		return ModelFactory.getFactory().getListClone(getDB());
	}

	private int getNextId() {
		return _seq++;
	}

}
