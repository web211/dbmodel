package dhbw.web.model.db;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Entity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = true)
public class CommentDB extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String text;
	private Date created;
	private int owner;
	private int parentId;
	private String parentType;
	@Setter(value = AccessLevel.NONE)
	private List<Integer> likes;

	public List<Integer> getLikes() {
		if (this.likes == null) {
			this.likes = ModelFactory.getFactory().getList();
		}
		return this.likes;
	}

}
