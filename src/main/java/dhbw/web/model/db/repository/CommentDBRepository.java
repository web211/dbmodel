package dhbw.web.model.db.repository;

import java.util.List;
import java.util.stream.Collectors;

import dhbw.web.model.db.CommentDB;

public class CommentDBRepository extends StaticDB<CommentDB> {

	public List<CommentDB> findByElement(int element, String type) {
		return getDB().stream().filter(x -> x.getParentId() == element && x.getParentType().equals(type))
				.collect(Collectors.toList());
	}
}
