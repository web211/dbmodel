package dhbw.web.model.db;

import java.util.Date;
import java.util.List;

import dhbw.web.interfaces.ModelFactory;
import dhbw.web.model.Entity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDB extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String email;
	private String firstname;
	private String lastname;
	private String password;
	private Date lastLogon;
	@Setter(value = AccessLevel.NONE)
	private List<Integer> likes;
	@Setter(value = AccessLevel.NONE)
	private List<Integer> follower;
	@Setter(value = AccessLevel.NONE)
	private List<Integer> comments;

	public List<Integer> getLikes() {
		if (this.likes == null) {
			this.likes = ModelFactory.getFactory().getList();
		}
		return this.likes;
	}

	public List<Integer> getComments() {
		if (this.comments == null) {
			this.comments = ModelFactory.getFactory().getList();
		}
		return this.comments;
	}

	public List<Integer> getFollower() {
		if (this.follower == null) {
			this.follower = ModelFactory.getFactory().getList();
		}
		return this.follower;
	}

}
