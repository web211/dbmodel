package dhbw.web.model.db.mapper;

import dhbw.web.interfaces.Mapper;
import dhbw.web.utils.MapperWrapper;

public abstract class ModelDBSelfreference<FROM, TO> extends Mapper<FROM, TO> {

	@Override
	protected MapperWrapper constrctMapper() {
		return ModelDBMapper.getModelDBMapper();
	}

}
