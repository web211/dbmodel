package dhbw.web.model.db.repository;

import java.util.NoSuchElementException;

import dhbw.web.model.db.QuestionDB;

public class QuestionDBRepository extends StaticDB<QuestionDB> {

	public QuestionDB findByUrl(String url) {
		try {
			return getDB().stream().filter(x -> x.getUrl().equals(url.trim())).findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

}
