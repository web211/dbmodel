package dhbw.web.model.db.mapper;

import dhbw.web.model.Follow;
import dhbw.web.model.Notification;
import dhbw.web.model.db.NotificationDB;

public class NotificationDBMapper extends ModelDBSelfreference<Notification, NotificationDB> {
	@Override
	public NotificationDB map(Notification o) {
		NotificationDB t = new NotificationDB();
		t.setCreated(o.getCreated());
		t.setText(o.getText());
		t.setFollow(o.getFollow().getId());
		t.setSeen(o.isSeen());
		t.setId(o.getId());
		return t;
	}

	@Override
	public Notification revert(NotificationDB t) {
		Notification o = new Notification();
		o.setCreated(t.getCreated());
		Follow f = new Follow();
		f.setId(t.getFollow());
		o.setFollow(f);
		o.setSeen(t.isSeen());
		o.setId(t.getId());
		return o;
	}
}
