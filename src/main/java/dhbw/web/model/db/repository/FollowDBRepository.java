package dhbw.web.model.db.repository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import dhbw.web.model.db.FollowDB;

public class FollowDBRepository extends StaticDB<FollowDB> {
	public FollowDB findByUserAndElement(int userId, int element, String type) {
		try {
			return getDB().stream()
					.filter(x -> x.getUser() == userId && x.getParentId() == element && x.getParentType().equals(type))
					.findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public List<FollowDB> findByElement(int element, String type) {
		return getDB().stream().filter(x -> x.getParentId() == element && x.getParentType().equals(type))
				.collect(Collectors.toList());
	}

	public List<FollowDB> findAllUserFollows(int userId) {
		return getDB().stream().filter(x -> x.getUser() == userId).collect(Collectors.toList());
	}
}
